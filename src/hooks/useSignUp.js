import { useState } from "react";
import { useAuthContext } from "./useAuthContext";

export const useSignup = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const { dispatch } = useAuthContext();

  const signup = async (firstName, lastName, email, phone, password) => {
    setIsLoading(true);
    setError(null);

    const response = await fetch(process.env.REACT_APP_API_URL + "/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user: {
          first_name: firstName,
          last_name: lastName,
          email: email,
          phone: phone,
          password: password,
        },
      }),
    });
    const json = await response.json();
    console.log("JSON: ", JSON.stringify(json));

    if (!response.ok) {
      setIsLoading(false);
      setError(json.error.message);
    }
    if (response.ok) {
      localStorage.setItem("user", JSON.stringify(json.user));
      dispatch({ type: "LOGIN", payload: json.user });
      setIsLoading(false);
      return true;
    }
  };

  return { signup, isLoading, error };
};
